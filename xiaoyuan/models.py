from django.contrib.gis.db import models

# Create your models here.
class poi(models.Model):
    # Regular Django fields corresponding to the attributes in the
    # world borders shapefile.
    name = models.CharField(max_length=50)
    type = models.CharField(max_length=50)
    # GeoDjango-specific: a geometry field (PointField)
    geom = models.PointField()

    # Returns the string representation of the model.
    def __str__(self):
        return self.name

class road(models.Model):
    geom = models.MultiLineStringField()
    def __str__(self):
        return self.name

class building(models.Model):
    name = models.CharField(max_length=50)
    geom = models.MultiPolygonField()
    def __str__(self):
        return self.name



class user(models.Model):
    username = models.CharField(max_length=50, unique=True)
    role = models.CharField(max_length=50)
    prefix = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    major = models.CharField(max_length=50)
    confirm = models.CharField(max_length=50)
    className = models.CharField(max_length=50)

    # class Meta:
    #     unique_together = ("field1", "field2")
    def __str__(self):
        return self.name
