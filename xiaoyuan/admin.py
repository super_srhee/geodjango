from django.contrib.gis import admin

# Register your models here.
from .models import poi,road,building

admin.site.register(poi, admin.GeoModelAdmin) # OSMGeoAdmin
admin.site.register(road, admin.OSMGeoAdmin)
admin.site.register(building, admin.OSMGeoAdmin)