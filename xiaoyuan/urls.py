# 引入需要的模块
from django.conf.urls import url
from django.urls import path
from . import views


urlpatterns = [
    path('testGet/', views.TestGet, name='Test'),  #test
    path('viewPoi/', views.ViewPoi, name='ViewPoi'),  # poi
    path('viewRoad/', views.ViewRoad, name='ViewPoi'),  # road
    path('viewBuilding/', views.ViewBuilding, name='ViewPoi'),  # building

    path('user/register', views.UserRegister, name='userRegister'),  # register
    path('user/login', views.UserLogin, name='userLogin'),  # login
]