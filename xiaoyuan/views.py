from django.shortcuts import render
from django.core.serializers import serialize
from .models import poi,road,building,user
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
import logging,time, os,json

logger = logging.getLogger('api.log')

#查
from util.ResponseHandler import MyJsonResponse


def TestGet(request):
    try:
        if request.method == 'POST':
            return MyJsonResponse().fail('please use post request to fetch data.')
        else:
            pois = serialize('geojson',poi.objects.all())
            print(pois)
            return MyJsonResponse().success(data=pois)
    except Exception as ex:
        return MyJsonResponse().fail(message=ex);

# 以geojson形式展现地图
def ViewPoi(request):
    try:
        if request.method == 'POST':
            return MyJsonResponse().fail('please use post request to fetch data.')
        else:
            pois = serialize('geojson',poi.objects.all())
            return MyJsonResponse().success(data=pois)
    except Exception as ex:
        return MyJsonResponse().fail(message=ex);

def ViewRoad(request):
    try:
        if request.method == 'POST':
            return MyJsonResponse().fail('please use post request to fetch data.')
        else:
            roads = serialize('geojson',road.objects.all())
            return MyJsonResponse().success(data=roads)
    except Exception as ex:
        return MyJsonResponse().fail(message=ex);

def ViewBuilding(request):
    try:
        if request.method == 'POST':
            return MyJsonResponse().fail('please use post request to fetch data.')
        else:
            buildings = serialize('geojson',building.objects.all())
            return MyJsonResponse().success(data=buildings)
    except Exception as ex:
        return MyJsonResponse().fail(message=ex);


@csrf_exempt
def UserLogin(request):
    try:
        if request.method == 'GET':
            return MyJsonResponse().fail('please use post request to fetch data.')
        else:
            requestData = json.loads(request.body)
            iuser = user.objects.filter(username=requestData['username'],password=requestData['password'])
            return MyJsonResponse().success(data=serialize('json',iuser))
    except Exception as ex:
        return MyJsonResponse().fail(message=ex)

@csrf_exempt
def UserRegister(request):
    try:
        if request.method == 'GET':
            return MyJsonResponse().fail('please use post request to fetch data.')
        else:
            requestData = json.loads(request.body)
            iuser = user(className=requestData["className"],
                         role=requestData["role"],
                         prefix=requestData["prefix"],
                         password=requestData["password"],
                         major=requestData["major"],
                         confirm=requestData["confirm"],
                         username=requestData["username"],
                         )
            iuser.save()
            # logger.info('-------className-------')
            # logger.info(className)
            return MyJsonResponse().success(data=requestData)
    except Exception as ex:
        return MyJsonResponse().fail(message=ex)
