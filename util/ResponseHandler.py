from django.http import JsonResponse
class HttpCode(object):
    ok = 200
    paramserror = 400
    unauth = 401
    methoderror = 405
    servererror = 500

class MyJsonResponse:
    def __init__(self):
        pass

    def ok(self):
        return result()
    def params_error(self,message="",data=None):
        return result(code=HttpCode.paramserror,message=message,data=data,kwargs={"result":'fail'})
    def unauth(self,message="",data=None):
        return result(code=HttpCode.unauth,message=message,data=data,kwargs={"result":'fail'})
    def method_error(self,message="",data=None):
        return result(code=HttpCode.methoderror,message=message,data=data,kwargs={"result":'fail'})
    def server_error(self,message="",data=None):
        return result(code=HttpCode.servererror,message=message,data=data,kwargs={"result":'fail'})

    def success(self):
        return result(kwargs={"result":'success',"success":True})
    def success(self,data=None):
        return result(HttpCode.ok,data= data,kwargs={"result":'success',"success":True})
    def success(self, message="", data=None):
        return result(HttpCode.ok, message=message, data=data, kwargs={"result": 'success',"success":True})

    def fail(self):
        return result(kwargs={"result":'fail',"success":False})
    def fail(self,message=""):
        return result(HttpCode.servererror,message=message,kwargs={"result":'fail',"success":False})
    def fail(self,data=None):
        return result(HttpCode.servererror,data=data,kwargs={"result":'fail',"success":False})
    def fail(self,message="",data=None):
        return result(HttpCode.servererror,message=message,data=data,kwargs={"result":'fail',"success":False})


def result(code=HttpCode.ok, message="", data=None, kwargs=None):
    json_dict = {"code":code, "message":message,"data":data}
    if kwargs and isinstance(kwargs, dict) and kwargs.keys():
        json_dict.update(kwargs)
    return JsonResponse(json_dict)